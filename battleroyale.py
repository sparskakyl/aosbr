# Battle royale
# Made by Lykakspars

# WARNING:
# THIS IS JUST ACTUALLY SPAGHETTI CODE
# AND IT'S POORLY-CODED
# YOU CAN CLEAN IT UP

# Import some stuff.
from pyspades.constants import *
from random import *
from twisted.internet import reactor
from twisted.internet.task import LoopingCall
from twisted.internet.reactor import callLater

BR_Map_Size = 511 # It's measured.

# Dict Documentary BS:
# 'CanSpawn': False
# 'Falling': False - to prevent fall damage.
# 'IsInArea': False 
# 'Killed': False 
# 'Timer': ???
# 'Spawned': False

def point_distance2D(c1x, c1y, c2x, c2y):
    return (c1x - c2x)**2 + (c1y - c2y)**2

def apply_script(protocol, connection, config):
	class BRConnection(connection):
		def spawn_check(self):
			if self.name is None or self not in self.protocol.players:
				return
			brd = self.protocol.BR_Dict[self]
			if brd['CanSpawn'] is False:
				return False
		
		# No capturing.
		def on_flag_take(self):
			self.send_chat("Intel disabled.")
			return False
			
		def on_refill(self):
			self.send_chat("Refilling disabled.")
			return False
		
		def on_spawn_location(self, pos):
			return randrange(BR_Map_Size+1), randrange(BR_Map_Size+1), -100
		
		def on_join(self):
			self.protocol.BR_Dict[self] = {
				'CanSpawn': True,
				'Falling': False,
				'IsInArea': True,
				'Killed': False,
				'Spawned': False,
			}
			if self.protocol.BR_Start is False:
				self.protocol.BR_Dict[self]['CanSpawn'] = True
			
		def on_team_leave(self):
			if not self.protocol.BR_Dict[self] is None:
				del self.protocol.BR_Dict[self]
			self.protocol.check_end_game(self)
		
		def on_spawn(self, pos):
			brd = self.protocol.BR_Dict[self]
			if not brd['CanSpawn']:
				self.kill()
				if self.protocol.BR_Start:
					brd['Timer'] = callLater(10.0, self.spawn_check)
			return connection.on_spawn(self, pos)
		
		def on_fall(self, damage):
			brd = self.protocol.BR_Dict[self] 
			if brd['Falling'] is True:
				brd['Falling'] = False
				return False
			return connection.on_fall(self, damage)
		
		def on_kill(self, killer, type, grenade):
			# print(dir(killer)) - for debugging purposes
			# Avoid crashing the server by commit suicide.
			if killer is None:
				self.protocol.check_end_game(self)
			else:
				self.protocol.check_end_game(killer)
			brd = self.protocol.BR_Dict[self] 
			brd['Killed'] = True
			brd['CanSpawn'] = False
			brd['Spawned'] = False
			for player in self.players.values():
				player.send_chat("Alive: %1" % self.protocol.count_spawned())
			return connection.on_kill(self, killer, type, grenade)
		
		def on_team_join(self, team):
			print("[BR]: %s spawned!" % self.name)
			# print(team.count()) - for debugging purposes
			brd = self.protocol.BR_Dict[self] 
			brd['Falling'] = True
			brd['Spawned'] = True
			return connection.on_team_join(self, team)
			
		def on_position_update(self):
			brd = self.protocol.BR_Dict[self] 
			if not self.protocol.IsInArea(self):
				self.hit(2)
				if brd['IsInArea'] is True:
					self.send_chat("You're restricting area, get back quickly!")
					brd['IsInArea'] = False
			else:
				brd['IsInArea'] = True
			
	class BRProtocol(protocol):
		game_mode = CTF_MODE
		BR_Area_Size = 131633
		BR_Dict = {}
		BR_Start = False
		BR_AreaShrink = None
		BR_Win = False
		
		def on_map_change(self, map):
			self.BR_Area_Size = 131633
			self.BR_Dict = {}
			self.BR_Start = False
			self.BR_Win = False
			if not self.BR_AreaShrink is None:
				self.BR_AreaShrink.stop()
			self.BR_AreaShrink = None
			print("[BR]: Map changed, wait for it to start")
			callLater(25.0, self.start_br)
			return protocol.on_map_change(self, map)
		
		def on_flag_spawn(self, x, y, z, flag, entity_id):
			if entity_id == GREEN_FLAG:
				return (0, 0, 63)
			if entity_id == BLUE_FLAG:
				return (0, 0, 63)
			return protocol.on_flag_spawn(self, x, y, z, flag, entity_id)
		
		def start_br(self):
			print(self.count_players())
			if self.count_spawned() == 0:
				print("[BR]: We're not ready yet, I need players to start!")
				callLater(25.0, self.start_br)
				return
			print("[BR]: Started!")
			for player in self.players.values():
				player.send_chat("Battle royale started!")
				pbrd = self.BR_Dict[player]
				pbrd['CanSpawn'] = False
			self.BR_Start = True
			self.BR_AreaShrink = LoopingCall(self.shrink_area)
			self.BR_AreaShrink.start(1)
			
		def shrink_area(self):
			countsy = 0
			for player in self.players.values():
				pbrd = self.BR_Dict[player]
				if pbrd['Killed'] is False:
					countsy += 1
			self.BR_Area_Size -= 2048
			if self.BR_Area_Size <= 3042*countsy:
				self.BR_Area_Size = 3042*countsy
				return
			print("[BR]: Shrinked into %i" % self.BR_Area_Size)
			return
		
		def check_end_game(self, player):
			# If every enemies is dead, congrats!
			if self.BR_Win is True:
				return
			countsy = 0
			for player in self.players.values():
				pbrd = self.BR_Dict[player]
				if pbrd['Killed'] is False:
					countsy += 1
			if countsy <= 2:
				self.BR_Start = False
				self.BR_Win = True
				if not self.BR_AreaShrink is None:
					self.BR_AreaShrink.stop()
				for player in self.players.values():
					pbrd = self.BR_Dict[player]
					if pbrd['CanSpawn'] is False:
						pbrd['CanSpawn'] = True
				if not player is None:
					if not self.get_last_players_alive() is None:
						self.reset_game(self.get_last_players_alive())
					else:
						self.reset_game()
				print("[BR]: %s won the game!" % player.name)
				protocol.on_game_end(self)
				return True
			return False
		
		def IsInArea(self, player):
			# print(point_distance2D(X, Y, BR_Map_Size/2, BR_Map_Size/2)) - for debugging purposes
			playerpos = player.world_object.position
			dist = point_distance2D(playerpos.x, playerpos.y, BR_Map_Size/2, BR_Map_Size/2)
			if dist <= self.BR_Area_Size:
				return True
			return False
		
		def count_players(self):
			result = 0
			for player in self.players.values():
				result += 1
			return result
			
		def count_spawned(self):
			result = 0
			for player in self.players.values():
				pbrd = self.BR_Dict[player]
				if pbrd['Spawned'] is True:
					result += 1
			return result
			
		def get_last_players_alive(self):
			for player in self.players.values():
				pbrd = self.BR_Dict[player]
				if pbrd['Killed'] is False:
					return player
			return None
	
	return BRProtocol, BRConnection